import { UserComponent } from "../app/UserComponent";
import { getStore } from "../app/store";
import { NoteComponent } from "../app/NoteComponent";



export function run_test() {

	console.log('******* TEST ********');
	
	let userComp = new UserComponent()
	userComp.render()
	userComp.setUser({ name: "new user name"})

	let noteComp = new NoteComponent()
	noteComp.render()
	noteComp.updateNote({ title: "Updated title"})
}