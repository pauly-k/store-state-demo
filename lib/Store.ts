type ListenerFunc = () => void

export class Store<T> {
  private state: T
  private listeners: ListenerFunc[] = []
  constructor(initialState: T) {
    this.state = initialState
  }

  getState(): T {
    return this.state
  }

  getStateKey<K extends keyof T>(key: K) {
    return this.state[key]
  }

  setState(newState: T) {
    this.state = { ...newState }
    this.notifyListeners()
  }

  setStateKey<K extends keyof T>(key: K, value: T[K]) {
    let newState = {...this.state}
    newState[key] = value
    this.state = newState
    this.notifyListeners()
  }

  subscribe(listener: ListenerFunc) {
    this.listeners.push(listener)
  }

  private notifyListeners() {
    this.listeners.forEach(listener => listener())
  }

  unSubscribe(listener: ListenerFunc) {
    const existing = this.listeners.indexOf(listener)
    if (existing >= 0) {
      this.listeners.splice(existing, 1)
    }
  }
}
