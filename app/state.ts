import { AppState } from "./interface";

export const initialState: AppState = {
	user: { name: 'Blank User'},
	note: { title: 'Blank note'}
}