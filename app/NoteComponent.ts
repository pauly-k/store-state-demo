import { Note, AppState } from "./interface";
import { getStore } from "./store";


const store = getStore()

export class NoteComponent {
	private note: Note

	constructor() {
		this.note = store.getStateKey('note')
		store.subscribe(this.onStateChange)
	}

	onStateChange = () => {
		this.note = store.getState().note
		this.render()
	}

	updateNote = (note: Note) => {
		store.setStateKey('note', note)
	}

	render() {
		console.log('---- NOTE RENDR ----');
		console.log(`NOTE: ${this.note.title}`);
	}
}