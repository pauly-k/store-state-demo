import { getStore } from "./store";
import { User } from "./interface";

const store = getStore()
export class UserComponent {
	private user: User

	constructor() {
		this.user = store.getStateKey('user')
		store.subscribe(this.onStateChange)
	}

	onStateChange = () => {
		this.user = store.getStateKey('user')
		this.render()
	}

	setUser(user: User) {
		store.setStateKey('user', user)
	}

	render() {
		console.log(`USER: ${this.user.name}`);
	}
}