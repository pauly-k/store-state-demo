

export interface User {
	name: string
}

export interface Note {
	title: string
}

export interface AppState {
	user: User
	note: Note
}