import { Store } from "../lib/Store";
import { AppState } from "./interface";
import { initialState } from "./state";


const store = new Store<AppState>(initialState) 

export function getStore() {
	return store
}
